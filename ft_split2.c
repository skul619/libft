/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jpark <jpark@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/02/19 10:52:02 by jpark             #+#    #+#             */
/*   Updated: 2024/02/19 12:04:42 by jpark            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int ft_count_words(const char *s, char c)
{
  int i;
  int words;

  i = 0;
  words = 0;
  while (s[i])
  {
    while (s[i] && s[i] == c)
      i++;
    if (s[i])
      words++;
    while (s[i] && s[i] != c)
      i++;
  }
  return (words);
}

size_t ft_strlen_sep(char *str, char c)
{
  size_t i;

  i = 0;
  while (str[i] && str[i] != c)
  {
    i++;
  }
  return (i);
}

size_t ft_strlen(char *str)
{
  int i;

  i = 0;
  while (str[i] != '\0')
  {
    i++;
  }
  return (i);
}

size_t  ft_strlcpy(char *dst, char *src, size_t size)
{
  size_t i;

  i = 0;
  if (size > 0)
  {
    while (src[i] && size - 1 > i)
    {
      dst[i] = src[i];
      i++;
    }
    dst[i] = '\0';
  }
  return (ft_strlen(src));
}

static char *ft_print_words(char const *s, char c)
{
  size_t lenstr;
  char *array;

  lenstr = ft_strlen_sep(s, c);
  array = (char *)malloc((lenstr + 1) * sizeof(char));
  ft_strlcpy(array, s, lenstr + 1);
  return (array);
}

static char **ft_allocate(char **array, char const *s, int count, char c)
{
  int i;
  int j;

  i = 0;
  j = 0;
  while (j < count && s[i])
  {
    while (s[i] && s[i] == c)
      i++;
    if (s[i])
    {
      array[j] = ft_print_words((s + i), c);
      if (!array[j])
      {
        while (j > 0)
          free(array[--j]);
        free(array);
        return (NULL);
      }
      j++;
    }
    i += ft_strlen_sep(s + i, c);
  }
  array[j] = 0;
  return (array);
}

char  **ft_split(char const *s, char c)
{
  char  **array;
  int   count;

  if (!s)
    return (NULL);
  count = ft_count_words(s, c);
  array = (char **)malloc((count + 1) * sizeof(char *));
  if (!array)
    return (NULL);
  array = ft_allocate(array, s, count, c);
  return (array);
}